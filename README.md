## Swift Remote
An Android remote control for the media center Plex.

## TODO
- Plex Home Theater clients can be discovered on the LAN with GDM, in the same way
    Plex servers are discovered today. **PlexClientGDMFinder.java** is already implemented, but is currently
    not in use. I'm unsure how this new functionality is best added to the GUI.
- Add ability to enter a Plex client's ip address manually.
- Add support to change subtitle/audio track on Plex Home Theater

