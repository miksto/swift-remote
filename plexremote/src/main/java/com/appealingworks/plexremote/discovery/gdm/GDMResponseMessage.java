/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.discovery.gdm;

import java.net.InetAddress;

public class GDMResponseMessage {
    private final String content;
    private final InetAddress mSourceInetAddress;

    public GDMResponseMessage(String content, InetAddress source) {
        this.content = content;
        this.mSourceInetAddress = source;
    }

    @Override
    public String toString() {
        return "GDMResponseMessage [content=" + content + ", mSourceInetAddress=" + mSourceInetAddress
                + "]";
    }

    public String getContent() {
        return content;
    }

    public InetAddress getSource() {
        return mSourceInetAddress;
    }


}
