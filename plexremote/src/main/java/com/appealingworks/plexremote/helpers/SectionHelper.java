/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appealingworks.plexremote.PlexRemoteApp;
import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.model.PlexSection;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.net.AsyncFailureCallback;
import com.appealingworks.plexremote.volley.requests.PlexSectionsRequest;
import com.appealingworks.plexremote.volley.requests.SectionRefreshRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikael Stockman on 08/12/13.
 */
public class SectionHelper {

    private static final String NORMAL_SCAN = "Normal scan";
    private static final String TURBO_SCAN = "Turbo scan";
    private static final String DEEP_SCAN = "Deep scan";
    private static final String FORCE_REFRESH = "Force refresh";
    private static AlertDialog sSectionChooserAlertDialog;
    private static AlertDialog sSectionRefreshModeAlertDialog;

    public static void getAndRefreshSection(final Context context,
                                            final PlexServer plexServer,
                                            final AsyncFailureCallback failureCallback) {
        PlexSectionsRequest request = new PlexSectionsRequest(
                plexServer,
                new Response.Listener<List<PlexSection>>() {
                    @Override
                    public void onResponse(List<PlexSection> plexSections) {
                        displaySectionChooserDialog(context, plexServer, plexSections, failureCallback);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        failureCallback.onFailure(volleyError.getCause());
                    }
                }
        );
        request.setTag(context);
        PlexRemoteApp.getApp().getVolleyRequestQueue().add(request);
    }

    private static void displaySectionChooserDialog(final Context context,
                                                    final PlexServer plexServer,
                                                    final List<PlexSection> plexSections,
                                                    final AsyncFailureCallback failureCallback) {
        if (plexSections != null) {
            List<String> sectionsNamesList = new ArrayList<String>(plexSections.size());
            for (PlexSection section : plexSections) {
                sectionsNamesList.add(section.getDisplayTitle(context));
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Refresh section:");
            builder.setItems(
                    sectionsNamesList.toArray(new String[sectionsNamesList.size()]),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int item) {
                            PlexSection section = plexSections.get(item);
                            displayRefreshModeDialog(context, plexServer, section, failureCallback);
                        }
                    }
            );
            sSectionChooserAlertDialog = builder.create();
            try {
                sSectionChooserAlertDialog.show();
            } catch (WindowManager.BadTokenException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, context.getString(R.string.noSections), Toast.LENGTH_SHORT);
        }
    }

    private static void displayRefreshModeDialog(final Context context,
                                                 final PlexServer plexServer,
                                                 final PlexSection plexSection,
                                                 final AsyncFailureCallback failureCallback) {

        final String[] refreshModes;
        if (plexServer.hasNewMediaScanner()) {
            refreshModes = new String[2];
            refreshModes[0] = NORMAL_SCAN;
            refreshModes[1] = FORCE_REFRESH;
        } else {
            refreshModes = new String[3];
            refreshModes[0] = TURBO_SCAN;
            refreshModes[1] = DEEP_SCAN;
            refreshModes[2] = FORCE_REFRESH;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.choose_mode));
        builder.setItems(
                refreshModes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        SectionRefreshRequest.RefreshMode mode;
                        if (refreshModes[item].equals(NORMAL_SCAN)) {
                            mode = SectionRefreshRequest.RefreshMode.Normal;
                        } else if (refreshModes[item].equals(DEEP_SCAN)) {
                            mode = SectionRefreshRequest.RefreshMode.Deep;
                        } else if (refreshModes[item].equals(FORCE_REFRESH)) {
                            mode = SectionRefreshRequest.RefreshMode.Force;
                        } else if (refreshModes[item].equals(TURBO_SCAN)) {
                            mode = SectionRefreshRequest.RefreshMode.Turbo;
                        } else {
                            mode = SectionRefreshRequest.RefreshMode.Turbo;
                        }

                        SectionRefreshRequest refreshRequest = new SectionRefreshRequest(
                                plexServer,
                                plexSection.getKey(),
                                mode,
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError volleyError) {
                                        failureCallback.onFailure(volleyError.getCause());
                                    }
                                }
                        );
                        refreshRequest.setTag(context);
                        PlexRemoteApp.getApp().getVolleyRequestQueue().add(refreshRequest);
                    }
                }
        );

        sSectionRefreshModeAlertDialog = builder.create();
        sSectionRefreshModeAlertDialog.show();
    }

    public static void dismissDialogs() {
        if (sSectionChooserAlertDialog != null) {
            sSectionChooserAlertDialog.dismiss();
            sSectionChooserAlertDialog = null;
        }

        if (sSectionRefreshModeAlertDialog != null) {
            sSectionRefreshModeAlertDialog.dismiss();
            sSectionRefreshModeAlertDialog = null;
        }
    }
}
