/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class AutoRepeatButton extends ImageView {

    private static final int INITIAL_REPEAT_DELAY_MS = 200;
    private static final int REPEAT_INTERVAL_MS = 50;

    public AutoRepeatButton(Context context) {
        super(context);
        init();
    }

    public AutoRepeatButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoRepeatButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    // Just to be sure that we removed all callbacks,
                    // which should have occurred in the ACTION_UP
                    removeCallbacks(mRepeatClickerRunnable);

                    // Schedule the start of repetitions after a one half second
                    // delay.
                    postDelayed(mRepeatClickerRunnable,
                            INITIAL_REPEAT_DELAY_MS);
                    return false;
                } else if (action == MotionEvent.ACTION_UP) {
                    // Cancel any repetition in progress.
                    removeCallbacks(mRepeatClickerRunnable);
                }
                return false;
            }
        });
    }

    public void stop() {
        removeCallbacks(mRepeatClickerRunnable);
    }

    private final Runnable mRepeatClickerRunnable = new Runnable() {
        @Override
        public void run() {
            // Perform the present repetition of the click action provided by
            // the user in setOnClickListener().
            long before = System.currentTimeMillis();
            performClick();
            long after = System.currentTimeMillis();
            long delay = REPEAT_INTERVAL_MS - Math.abs(after - before);
            if (delay < 0)
                delay = 0;

            // Schedule next click
            postDelayed(mRepeatClickerRunnable, delay);
        }
    };
}
