/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

public class PlexClient implements Serializable {
    private static final String PLEX_REMOTE_PROTOCOL_NAME = "plex";
    private static final String PLEX_VERSION_BIG_STEP_SUPPORT = "1.2.0";

    @SerializedName("name")
    private String mName;

    @SerializedName("host")
    private String mHost;

    @SerializedName("address")
    private String mAddress;

    @SerializedName("port")
    private String mPort;

    @SerializedName("version")
    private PlexVersion mPlexVersion;

    @SerializedName("protocol")
    private String mProtocol;

    @SerializedName("protocolVersion")
    private String mProtocolVersion;

    @SerializedName("product")
    private String mProduct;

    @SerializedName("deviceClass")
    private String mDeviceClass;

    public PlexClient(String name, String ipAddress) {
        this.mName = name;
        this.mHost = ipAddress;
        this.mPlexVersion = new PlexVersion(null);
    }

    public PlexClient() {
        this.mPlexVersion = new PlexVersion(null);
    }

    public static PlexClient parseFromHeaders(Map<String, String> headers) {
        PlexClient plexClient = new PlexClient();
        plexClient.setName(headers.get("Name"));
        plexClient.setPort(headers.get("Port"));
        plexClient.setProduct(headers.get("Product"));
        plexClient.setProtocol(headers.get("Protocol"));
        plexClient.setProtocolVersion(headers.get("Protocol-Version"));
        plexClient.setVersion(headers.get("Version"));
        return plexClient;
    }

    public String getUrl() {
        return "http://" + mHost + ":" + mPort;
    }

    public boolean supportsPlexRemoteProtocol() {
        if (mProduct == null) {
            return false;
        } else {
            return mProtocol.equalsIgnoreCase(PLEX_REMOTE_PROTOCOL_NAME);
        }
    }

    public boolean supportsBigSteps() {
        if (!supportsPlexRemoteProtocol()) {
            return true;
        }

        if (mPlexVersion == null) {
            return false;
        }

        if (mPlexVersion.isGreaterOrEqualTo(PLEX_VERSION_BIG_STEP_SUPPORT)) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return mName;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getHost() {
        return mHost;
    }

    public void setHost(String host) {
        this.mHost = host;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        this.mPort = port;
    }

    public PlexVersion getVersion() {
        return mPlexVersion;
    }

    public void setVersion(String version) {
        this.mPlexVersion = new PlexVersion(version);
    }

    public void setVersion(PlexVersion version) {
        this.mPlexVersion = version;
    }

    public String getProtocol() {
        return mProtocol;
    }

    public void setProtocol(String protocol) {
        this.mProtocol = protocol;
    }

    public String getProduct() {
        return mProduct;
    }

    public void setProduct(String product) {
        this.mProduct = product;
    }

    public String getDeviceClass() {
        return mDeviceClass;
    }

    public void setDeviceClass(String deviceClass) {
        this.mDeviceClass = deviceClass;
    }

    public String getProtocolVersion() {
        return mProtocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.mProtocolVersion = protocolVersion;
    }
}
