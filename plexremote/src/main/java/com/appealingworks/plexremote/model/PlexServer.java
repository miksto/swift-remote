/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

public class PlexServer implements Serializable {
    private static final String PREFS_SERVER_IP = "serverIP";
    private static final String PREFS_SERVER_PORT = "serverPort";
    private static final String PREFS_VERSION = "version";
    private static final String PREFS_NAME = "serverName";

    private static final String VERSION_NEW_MEDIA_SCANNER = "0.9.8.8";

    private String mIpAddress;
    @SerializedName("friendlyName")
    private String mName;
    private int mPortNr;
    @SerializedName("version")
    private PlexVersion mPlexVersion;

    public PlexServer() {
        mPlexVersion = new PlexVersion(null);
    }

    public static PlexServer parseFromHeaders(Map<String, String> headers) {
        PlexServer plexServer = new PlexServer();
        int port;
        try {
            port = Integer.parseInt(headers.get("Port"));
        } catch (NumberFormatException e) {
            // Default to standard port number
            port = 32400;
        }
        plexServer.setPortNr(port);
        plexServer.setName(headers.get("Name"));
        plexServer.setVersion(headers.get("Version"));
        return plexServer;
    }

    public static PlexServer getStoredPlexServer(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        PlexServer plexServer = new PlexServer();
        plexServer.setIpAddress(prefs.getString(PREFS_SERVER_IP, "192.168.1.0"));
        plexServer.setPortNr(prefs.getInt(PREFS_SERVER_PORT, 32400));
        plexServer.setName(prefs.getString(PREFS_NAME, ""));
        plexServer.setVersion(prefs.getString(PREFS_VERSION, ""));

        return plexServer;
    }

    public static void storePlexServer(Context context, PlexServer plexServer) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(PREFS_SERVER_PORT, plexServer.getPortNr());
        edit.putString(PREFS_SERVER_IP, plexServer.getIpAddress());
        edit.putString(PREFS_VERSION, plexServer.getVersion().getVersionString());
        edit.putString(PREFS_NAME, plexServer.getName());
        edit.commit();
    }

    public boolean hasNewMediaScanner() {
        return mPlexVersion.isGreaterOrEqualTo(VERSION_NEW_MEDIA_SCANNER);
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getIpAddress() {
        return mIpAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.mIpAddress = ipAddress;
    }

    public int getPortNr() {
        return mPortNr;
    }

    public void setPortNr(int portNr) {
        this.mPortNr = portNr;
    }

    public String getUrl() {
        return "http://" + mIpAddress + ":" + mPortNr;
    }

    public PlexVersion getVersion() {
        return mPlexVersion;
    }

    public void setVersion(PlexVersion version) {
        this.mPlexVersion = version;
    }

    public void setVersion(String version) {
        setVersion(new PlexVersion(version));
    }

    @Override
    public String toString() {
        return mName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || PlexServer.class != o.getClass()) return false;

        PlexServer that = (PlexServer) o;

        if (mPortNr != that.mPortNr) {
            return false;
        }
        if (mIpAddress != null ? !mIpAddress.equals(that.mIpAddress) : that.mIpAddress != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = mIpAddress != null ? mIpAddress.hashCode() : 0;
        result = 31 * result + mPortNr;
        return result;
    }
}
