/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.os.Bundle;
import android.support.v4.view.VelocityTrackerCompat;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

import com.appealingworks.plexremote.util.Log;

public class ScrollTouchpadFragment extends AbstractTouchpadFragment {

    // The gesture threshold expressed in dp
    private static final float GESTURE_THRESHOLD_DP = 17.0f;
    private static final int INITIAL_FLINGSCROLLER_DELAY = 50;
    private final Object mSyncObj = new Object();
    private float mLastScrollY, mLastScrollX;
    private VelocityTracker mVelocityTracker;
    private Movement mLatestMovementDirection;
    private boolean mIsStopping;

    private boolean flingScrollerAborted;
    private final GestureDetector mGestureDetector = new GestureDetector(getActivity(),
            new OnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    mCommandSender.select();
                    return false;
                }

                @Override
                public void onShowPress(MotionEvent e) {

                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                        float distanceX, float distanceY) {
                    int index = e2.getActionIndex();
                    // int action = e2.getActionMasked();
                    int pointerId = e2.getPointerId(index);

                    float dpYDistSinceLastCommand = Math
                            .abs(dpFromPx(mLastScrollY - e2.getY()));
                    float dpXDistSinceLastCommand = Math
                            .abs(dpFromPx(mLastScrollX - e2.getX()));

                    /* Vertical movement */
                    if (dpYDistSinceLastCommand > dpXDistSinceLastCommand) {
                        mVelocityTracker.computeCurrentVelocity(1000);

                        float dpYVelocity = dpFromPx(VelocityTrackerCompat
                                .getYVelocity(mVelocityTracker, pointerId));

                        if (shouldSendCommand(dpYDistSinceLastCommand,
                                dpYVelocity)) {

                            // left --> right
                            if (mLastScrollY < e2.getY()) {
                                mLatestMovementDirection = Movement.DOWN;
                                mCommandSender.down();
                            }
                            // right --> left
                            else {
                                mLatestMovementDirection = Movement.UP;
                                mCommandSender.up();
                            }
                            mLastScrollY = e2.getY();
                            mLastScrollX = e2.getX();
                        }
                    }

                    /* Horizontal movement */
                    else {
                        mVelocityTracker.computeCurrentVelocity(1000);
                        float dpXVelocity = dpFromPx(VelocityTrackerCompat
                                .getXVelocity(mVelocityTracker, pointerId));

                        if (shouldSendCommand(dpXDistSinceLastCommand,
                                dpXVelocity)) {

                            // left --> right
                            if (mLastScrollX < e2.getX()) {
                                mLatestMovementDirection = Movement.RIGHT;
                                mCommandSender.right();
                            }
                            // right --> left
                            else {
                                mLatestMovementDirection = Movement.LEFT;
                                mCommandSender.left();
                            }
                            mLastScrollY = e2.getY();
                            mLastScrollX = e2.getX();
                        }
                    }

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2,
                                       float velocityX, float velocityY) {
                    // float yDist = Math.abs(e1.getY() - e2.getY());
                    // float xDist = Math.abs(e1.getX() - e2.getX());
                    Direction direction = null;
                    float dpVelocity = 0, dpDist = 0;

                    if (mLatestMovementDirection == Movement.DOWN) {
                        direction = Direction.DOWN;
                        dpVelocity = dpFromPx(velocityY);
                        dpDist = dpFromPx(e2.getY() - e1.getY());
                    } else if (mLatestMovementDirection == Movement.UP) {
                        direction = Direction.UP;
                        dpVelocity = dpFromPx(velocityY);
                        dpDist = dpFromPx(e1.getY() - e2.getY());
                    } else if (mLatestMovementDirection == Movement.LEFT) {
                        direction = Direction.LEFT;
                        dpVelocity = dpFromPx(velocityX);
                        dpDist = dpFromPx(e2.getX() - e1.getX());
                    } else if (mLatestMovementDirection == Movement.RIGHT) {
                        direction = Direction.RIGHT;
                        dpVelocity = dpFromPx(velocityX);
                        dpDist = dpFromPx(e1.getX() - e2.getX());
                    }
                    dpVelocity = Math.abs(dpVelocity);

                    Log.v(TAG, "DPDIST: " + dpDist);
                    if (dpVelocity > 500 && dpDist > 140 && direction != null) {

                        int nextDelay = INITIAL_FLINGSCROLLER_DELAY;
                        float delayScaleFactor = Math.max(250f / dpVelocity, 1);
                        nextDelay *= delayScaleFactor;
                        Log.d(TAG, "DELAY: " + nextDelay);
                        Runnable runnable = new FlingScrollerRunnable(direction,
                                nextDelay);
                        Log.v(TAG, "Vel: " + dpVelocity);
                        flingScrollerAborted = false;
                        mHandler.post(runnable);
                        Log.w(TAG, "nextDelay: " + nextDelay);
                    }
                    return true;
                }

                @Override
                public boolean onDown(MotionEvent e) {
                    mLastScrollY = e.getY();
                    mLastScrollX = e.getX();

                    return false;
                }
            }
    );

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsStopping = false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            mVelocityTracker.addMovement(event);
        }
        if (event.getAction() == MotionEvent.ACTION_UP
                || event.getAction() == MotionEvent.ACTION_CANCEL) {
            mVelocityTracker.recycle();
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            flingScrollerAborted = true;
            mVelocityTracker = VelocityTracker.obtain();
        }

        mGestureDetector.onTouchEvent(event);
        return true;
    }

    private boolean shouldSendCommand(float dpDist, float dpVelocity) {
        return Math.abs(dpDist) > getGestureThreshold(Math.abs(dpVelocity));
    }

    private float getGestureThreshold(float dpVelocity) {
        int velocity = (int) (dpVelocity / 110f);
        return Math.max(4, GESTURE_THRESHOLD_DP - velocity);
    }

    @Override
    public void onStop() {
        synchronized (mSyncObj) {
            mIsStopping = true;
        }
        super.onStop();
    }

    private enum Movement {
        DOWN, UP, LEFT, RIGHT
    }

    private class FlingScrollerRunnable implements Runnable {
        private final Direction direction;
        private final int nextDelay;

        public FlingScrollerRunnable(Direction direction, int nextDelay) {
            this.direction = direction;
            this.nextDelay = nextDelay;
        }

        @Override
        public void run() {
            synchronized (mSyncObj) {
                if (!mIsStopping) {
                    switch (direction) {
                        case DOWN:
                            mCommandSender.down();
                            break;
                        case UP:
                            mCommandSender.up();
                            break;
                        case LEFT:
                            mCommandSender.left();
                            break;
                        case RIGHT:
                            mCommandSender.right();
                            break;
                    }

                    if (nextDelay < 200 && !flingScrollerAborted) {
                        FlingScrollerRunnable flingScrollerRunnable = new FlingScrollerRunnable(
                                direction, (int) (nextDelay * 1.3));
                        mHandler.postDelayed(flingScrollerRunnable, nextDelay);
                    }
                }
            }
        }
    }
}
