/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.support.v4.app.Fragment;

import com.appealingworks.plexremote.commandsender.CommandSender;
import com.appealingworks.plexremote.net.AsyncFailureCallback;

public abstract class AbstractNavigationFragment extends Fragment {
    protected final String TAG = getClass().getSimpleName();

    protected CommandSender mCommandSender;
    protected AsyncFailureCallback mAsyncFailureCallback;

    public void setCommandSender(CommandSender commandSender) {
        this.mCommandSender = commandSender;
    }

    public void setAsyncFailureCallback(AsyncFailureCallback asyncFailureCallback) {
        this.mAsyncFailureCallback = asyncFailureCallback;
    }

    @Override
    public void onDestroy() {
        mAsyncFailureCallback = null;
        mCommandSender = null;

        super.onDestroy();
    }

}
