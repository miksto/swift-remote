/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.widget.AutoRepeatButton;

public class NavigationButtonsFragment extends AbstractNavigationFragment {

    private final OnClickListener mLeftButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.left();
        }
    };
    private final OnClickListener mUpButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.up();
        }
    };
    private final OnClickListener mRightButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.right();
        }
    };
    private final OnClickListener mDownButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.down();
        }
    };
    private final OnClickListener mSelectButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.select();
        }
    };
    private AutoRepeatButton mLeftButton, mUpButton, mRightButton, mDownButton;

    public static NavigationButtonsFragment newInstance() {
        return new NavigationButtonsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.component_main_buttons, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLeftButton = (AutoRepeatButton) getView().findViewById(
                R.id.button_left);
        mUpButton = (AutoRepeatButton) getView().findViewById(R.id.button_up);
        mRightButton = (AutoRepeatButton) getView().findViewById(
                R.id.button_right);
        mDownButton = (AutoRepeatButton) getView().findViewById(
                R.id.button_down);
        View selectButton = getView().findViewById(R.id.button_select);

        mLeftButton.setOnClickListener(mLeftButtonOnClickListener);
        mUpButton.setOnClickListener(mUpButtonOnClickListener);
        mRightButton.setOnClickListener(mRightButtonOnClickListener);
        mDownButton.setOnClickListener(mDownButtonOnClickListener);
        selectButton.setOnClickListener(mSelectButtonOnClickListener);
    }

    @Override
    public void onStop() {
        mUpButton.stop();
        mRightButton.stop();
        mDownButton.stop();
        mLeftButton.stop();

        super.onStop();
    }

}
