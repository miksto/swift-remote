/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.util.Constants;
import com.appealingworks.plexremote.util.PermissionUtil;

public class AppSettingsFragment extends PreferenceFragmentCompat {

    private static final int REQUEST_CODE_PERMISSION = 1;


    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Show all preferences and maybe deactivate some
        addPreferencesFromResource(R.xml.preferences);

        Preference myPref = findPreference(Constants.PREFS_KEY_VERSION);
        if (myPref != null) {
            myPref.setSummary(getPlexRemoteVersionText(getActivity()));
        }

        final CheckBoxPreference checkboxPref = (CheckBoxPreference) getPreferenceManager().findPreference("autopause");

        checkboxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((boolean) newValue) {
                    if (PermissionUtil.hasReadPhoneStatePermission(getActivity())) {
                        return true;
                    } else {
                        requestPermissions(PermissionUtil.PHONE_STATE_PERMISSION, REQUEST_CODE_PERMISSION);
                        return false;
                    }
                } else {
                    //No need to request permission when toggling the feature off
                    return true;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission granted, enable the feature
                    final CheckBoxPreference autoPauseOnCallPreference =
                            (CheckBoxPreference) getPreferenceManager().findPreference("autopause");
                    autoPauseOnCallPreference.setChecked(true);
                }
                //If permission was not granted, just leave the feature disabled
            }
        }
    }

    private static String getPlexRemoteVersionText(Context context) {
        String applicationName, versionName;
        applicationName = context.getResources().getString(R.string.app_name);

        try {
            versionName = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "--";
        }
        return applicationName + "\n" + versionName;
    }

}
