/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.commandsender;

import com.appealingworks.plexremote.Shutdownable;
import com.appealingworks.plexremote.commandsender.httpclient.CommandHttpClient;
import com.appealingworks.plexremote.commandsender.httpclient.ConnectionException;
import com.appealingworks.plexremote.net.AsyncFailureCallback;
import com.appealingworks.plexremote.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mikael Stockman on 08/02/14.
 */
public class CommandExecutorService implements Shutdownable {
    private static final String TAG = CommandExecutorService.class.getSimpleName();
    private static final int MAX_QUEUE_LENGTH = 4;

    private ExecutorService mExecutorService;
    private CommandHttpClient mHttpClient;
    private AtomicInteger mQueueCount;
    private boolean mShutdown;

    public CommandExecutorService(ExecutorService executorService, CommandHttpClient httpClient) {
        if (executorService == null) {
            throw new NullPointerException("mExecutorService cannot be null");
        }
        if (httpClient == null) {
            throw new NullPointerException("mHttpClient cannot be null");
        }

        this.mExecutorService = executorService;
        this.mHttpClient = httpClient;
        mQueueCount = new AtomicInteger(0);
        mShutdown = false;
    }

    public String execute(String command)
            throws ConnectionException, IllegalArgumentException {
        if (mShutdown) {
            throw new IllegalStateException("mShutdown() has been called");
        }
        if (command == null) {
            throw new NullPointerException("Command was null");
        }

        return mHttpClient.execute(command);
    }

    public void executeAsync(final String command, final AsyncFailureCallback callback) {
        if (mShutdown) {
            throw new IllegalStateException("mShutdown() has been called");
        }
        if (command == null && callback != null) {
            callback.onFailure(new NullPointerException("Command was null"));
        }

        if (mQueueCount.get() > MAX_QUEUE_LENGTH) {
            return;
        }
        mQueueCount.incrementAndGet();
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    mQueueCount.decrementAndGet();
                    if (!mShutdown) {
                        Log.v(TAG, "Command: " + command);
                        mHttpClient.executeAndConsume(command);
                    }
                } catch (Throwable e) {
                    if (!mShutdown && callback != null)
                        callback.onFailure(e);
                }
            }
        });
    }

    @Override
    public void shutdown() {
        if (mShutdown) {
            return;
        }
        mShutdown = true;
        if (mExecutorService != null) {
            mExecutorService.shutdownNow();
            mExecutorService = null;
        }

        if (mHttpClient != null) {
            mHttpClient.shutdown();
            mHttpClient = null;
        }
    }

    @Override
    public boolean isShutdown() {
        return mShutdown;
    }
}
