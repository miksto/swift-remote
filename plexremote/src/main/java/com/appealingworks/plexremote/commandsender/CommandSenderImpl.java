/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.commandsender;

import com.appealingworks.plexremote.commandsender.httpclient.CommandHttpClient;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.net.AsyncFailureCallback;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Mikael Stockman
 */
public class CommandSenderImpl implements CommandSender {
    private static final String TAG = CommandSenderImpl.class.getSimpleName();
    private final PlexClient mPlexClient;
    private final PlexServer mPlexServer;

    private boolean mShutdown;
    private CommandExecutorService mCommandExecutor;
    private String mCommandBaseURL;

    private AsyncFailureCallback mAsyncFailureCallback;
    private String mBackCommand;

    public CommandSenderImpl(CommandExecutorService executor, PlexServer plexServer,
                             PlexClient plexClient, AsyncFailureCallback callback) {

        if (executor == null)
            throw new NullPointerException("ExecutorService cannot be null");

        if (plexServer == null)
            throw new NullPointerException("PlexServer cannot be null");

        if (plexClient == null)
            throw new NullPointerException("PlexClient cannot be null");

        this.mPlexServer = plexServer;
        this.mPlexClient = plexClient;
        this.mAsyncFailureCallback = callback;

        if (plexClient.supportsPlexRemoteProtocol()) {
            mCommandBaseURL = this.mPlexClient.getUrl() + "/player";
        } else {
            mCommandBaseURL = this.mPlexServer.getUrl() + "/system/players/"
                    + this.mPlexClient.getAddress();
        }

        this.mCommandExecutor = executor;
        mShutdown = false;
    }

    public static CommandSenderImpl newInstance(PlexServer plexServer, PlexClient plexClient, AsyncFailureCallback callback) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        CommandHttpClient httpClient = CommandHttpClientFactory.create(CommandHttpClientFactory.Type.HttpUrlConnection);
        CommandExecutorService commandExecutorService = new CommandExecutorService(executorService, httpClient);

        return new CommandSenderImpl(commandExecutorService, plexServer, plexClient, callback);
    }

    private void sendAsyncCommand(final String command) {
        if (mShutdown) {
            throw new IllegalStateException("shutdown() has been called");
        }
        mCommandExecutor.executeAsync(mCommandBaseURL + command, mAsyncFailureCallback);
    }

    @Override
    public void shutdown() {
        if (mShutdown) {
            return;
        }
        mShutdown = true;

        if (mCommandExecutor != null) {
            mCommandExecutor.shutdown();
            mCommandExecutor = null;
        }
    }

    @Override
    public boolean isShutdown() {
        return mShutdown;
    }

    @Override
    public void up() {
        sendAsyncCommand(Command.UP);
    }

    @Override
    public void down() {
        sendAsyncCommand(Command.DOWN);
    }

    @Override
    public void left() {
        sendAsyncCommand(Command.LEFT);
    }

    @Override
    public void right() {
        sendAsyncCommand(Command.RIGHT);
    }

    @Override
    public void select() {
        sendAsyncCommand(Command.SELECT);
    }

    @Override
    public void back() {
        sendAsyncCommand(mBackCommand);
    }

    @Override
    public void play() {
        sendAsyncCommand(Command.PLAY + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void pause() {
        sendAsyncCommand(Command.PAUSE + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void stop() {
        sendAsyncCommand(Command.STOP + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void skipPrevious() {
        sendAsyncCommand(Command.SKIP_PREVIOUS + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void skipNext() {
        sendAsyncCommand(Command.SKIP_NEXT + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void bigStepForward() {
        sendAsyncCommand(Command.BIG_STEP_FORWARD);
    }

    @Override
    public void bigStepBack() {
        sendAsyncCommand(Command.BIG_STEP_BACK);
    }

    @Override
    public void stepForward() {
        sendAsyncCommand(Command.STEP_FORWARD + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void stepBack() {
        sendAsyncCommand(Command.STEP_BACK + (mPlexClient.supportsPlexRemoteProtocol() ? "?type=video" : ""));
    }

    @Override
    public void rewind() {
        sendAsyncCommand(Command.REWIND);
    }

    @Override
    public void fastForward() {
        sendAsyncCommand(Command.FASTFORWARD);
    }

    @Override
    public void showMenu() {
        sendAsyncCommand(Command.MENU);
    }

    @Override
    public void showOSD() {
        sendAsyncCommand(Command.OSD);
    }

    @Override
    public void showCodecInfo() {
        sendAsyncCommand(Command.CODEC_INFO);
    }

    @Override
    public void toggleSubtitle() {
        sendAsyncCommand(Command.SUBTITLES_TOGGLE);
    }

    @Override
    public void subtitleLanguage() {
        sendAsyncCommand(Command.SUBTITLES_LANGUAGE);
    }

    @Override
    public void audioLanguage() {
        sendAsyncCommand(Command.AUDIO_LANGUAGE);
    }

    @Override
    public void setVolume(int volume) {
        if (mPlexClient.supportsPlexRemoteProtocol()) {
            sendAsyncCommand("/playback/setParameters?type=video&volume=" + volume);
        } else {
            sendAsyncCommand(Command.SET_VOLUME + volume);
        }
    }

    @Override
    public void queue() {
        sendAsyncCommand(Command.QUEUE);
    }

    @Override
    public void sendText(String string) {

        try {
            sendAsyncCommand("/application/setText?text=" + URLEncoder.encode(string, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUseEscapeAsBack(boolean value) {
        mBackCommand = getBackCommand(value);
    }

    public String getBackCommand(boolean useEscapeForBack) {
        if (mPlexClient.supportsPlexRemoteProtocol()) {
            return Command.BACK;
        } else {
            return (useEscapeForBack ? Command.ESCAPE : Command.BACK);
        }
    }

    private static class Command {
        /* NAVIGATION COMMANDS */
        public static final String DOWN = "/navigation/moveDown";
        public static final String UP = "/navigation/moveUp";
        public static final String LEFT = "/navigation/moveLeft";
        public static final String RIGHT = "/navigation/moveRight";
        public static final String SELECT = "/navigation/select";
        public static final String MENU = "/navigation/contextMenu";
        public static final String OSD = "/navigation/toggleOSD";
        public static final String BACK = "/navigation/back";

        /* PLAYBACK COMMANDS */
        public static final String SKIP_PREVIOUS = "/playback/skipPrevious";
        public static final String SKIP_NEXT = "/playback/skipNext";
        public static final String BIG_STEP_FORWARD = "/playback/bigStepForward";
        public static final String BIG_STEP_BACK = "/playback/bigStepBack";
        public static final String STEP_FORWARD = "/playback/stepForward";
        public static final String STEP_BACK = "/playback/stepBack";
        public static final String REWIND = "/playback/rewind";
        public static final String FASTFORWARD = "/playback/fastForward";
        public static final String STOP = "/playback/stop";
        public static final String PAUSE = "/playback/pause";
        public static final String PLAY = "/playback/play";

        /* APPLICATION COMMANDS */
        public static final String ESCAPE = "/application/sendKey?code=27";
        public static final String SET_VOLUME = "/application/setVolume?level=";
        public static final String SUBTITLES_TOGGLE = "/application/sendKey?code=83";
        public static final String SUBTITLES_LANGUAGE = "/application/sendKey?code=76";
        public static final String CODEC_INFO = "/application/sendKey?code=73";
        public static final String AUDIO_LANGUAGE = "/application/sendKey?code=65";
        public static final String QUEUE = "/application/sendKey?code=81";

    }
}
