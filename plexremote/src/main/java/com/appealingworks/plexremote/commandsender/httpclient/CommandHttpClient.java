/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.commandsender.httpclient;


import com.appealingworks.plexremote.Shutdownable;

/**
 * @author Mikael Stockman
 */
public interface CommandHttpClient extends Shutdownable {

    /**
     * @param url the URL
     * @return The string representation of the content in the response
     * @throws ConnectionException      in case of a problem or the connection was aborted
     * @throws IllegalArgumentException if the url is invalid
     */
    public String execute(String url) throws ConnectionException,
            IllegalArgumentException;

    /**
     * Makes the http call, but immediately consumes the returned content
     * instead of parsing it.
     *
     * @param url the URL
     * @throws ConnectionException      in case of a problem or the connection was aborted
     * @throws IllegalArgumentException if the url is invalid
     */
    public void executeAndConsume(String url) throws ConnectionException,
            IllegalArgumentException;

    /**
     * Releases all resources
     */
    public void shutdown();
}
