/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.volley.requests;

import com.android.volley.Response;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.model.PlexVersion;
import com.appealingworks.plexremote.volley.responsemodels.ClientsResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by Mikael Stockman on 07/12/13.
 */
public class PlexClientRequest extends PlexGsonRequest<ClientsResponseModel> {

    public static final String PATH = "/clients";

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param plexServer    The plex server to retrieve clients from
     * @param listener
     * @param errorListener
     */
    public PlexClientRequest(PlexServer plexServer,
                             final Response.Listener<List<PlexClient>> listener,
                             Response.ErrorListener errorListener) {

        super(plexServer, PATH, ClientsResponseModel.class, new Response.Listener<ClientsResponseModel>() {
            @Override
            public void onResponse(ClientsResponseModel clientsResponseModel) {
                listener.onResponse(clientsResponseModel.getClientList());
            }
        }, errorListener);
    }

    @Override
    protected Gson createGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(PlexVersion.class, new PlexVersion.PlexVersionDeserializer());
        return gsonBuilder.enableComplexMapKeySerialization().create();
    }
}
