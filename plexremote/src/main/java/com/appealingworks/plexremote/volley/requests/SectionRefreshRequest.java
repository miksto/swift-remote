/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.volley.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.appealingworks.plexremote.model.PlexServer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mikael Stockman on 07/12/13.
 */
public class SectionRefreshRequest extends Request<String> {

    private final RefreshMode mRefreshMode;

    public SectionRefreshRequest(PlexServer plexServer, int sectionKey, RefreshMode refreshMode, Response.ErrorListener listener) {
        super(Method.GET, createUrl(plexServer, sectionKey), listener);
        mRefreshMode = refreshMode;
    }

    private static String createUrl(PlexServer plexServer, int sectionKey) {
        return plexServer.getUrl() + "/library/sections/" + sectionKey + "/refresh";
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>(1);
        switch (mRefreshMode) {

            case Force:
                headers.put("force", "1");
                break;
            case Deep:
                headers.put("deep", "1");
                break;
            case Normal:
            case Turbo:
                break;
        }
        return headers;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse networkResponse) {
        return Response.success("", HttpHeaderParser.parseCacheHeaders(networkResponse));
    }

    @Override
    protected void deliverResponse(String s) {
        //No need to deliver anything
    }

    public enum RefreshMode {
        Force,
        Deep,
        Normal,
        Turbo
    }
}
