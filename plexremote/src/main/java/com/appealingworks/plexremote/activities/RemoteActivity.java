/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appealingworks.plexremote.PlexRemoteApp;
import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.commandsender.CommandSender;
import com.appealingworks.plexremote.commandsender.CommandSenderImpl;
import com.appealingworks.plexremote.commandsender.httpclient.ConnectionException;
import com.appealingworks.plexremote.fragments.AbstractNavigationFragment;
import com.appealingworks.plexremote.fragments.JoystickTouchpadFragment;
import com.appealingworks.plexremote.fragments.NavigationButtonsFragment;
import com.appealingworks.plexremote.helpers.SectionHelper;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.net.AsyncFailureCallback;
import com.appealingworks.plexremote.util.Constants;
import com.appealingworks.plexremote.util.Log;

public class RemoteActivity extends AbstractPlexActivity implements
        OnSharedPreferenceChangeListener, AsyncFailureCallback {
    private static final String INTENT_PLEX_SERVER = "intent_plex_server";
    private static final String INTENT_PLEX_CLIENT = "intent_plex_client";

    private static final String TAG_TOUCHPAD_FRAGMENT = "tag_touchpad_fragment";
    private static final String TAG_NAVBUTTONS_FRAGMENT = "tag_navbuttons_fragment";
    private static final String TAG = RemoteActivity.class.getSimpleName();
    private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state) {
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    break;
                case TelephonyManager.CALL_STATE_RINGING:

                    Log.i(TAG, "Incoming call");

                    mCommandSender.pause();

                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    break;
            }
        }
    };
    private final OnClickListener mBackButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.back();
        }
    };
    private final OnClickListener mQuitButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            finish();
        }
    };
    private final OnClickListener mSkipPreviousButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.skipPrevious();
        }
    };
    private final OnClickListener mRewindButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.rewind();
        }
    };
    private final OnClickListener mFastForwardButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.fastForward();
        }
    };
    private final OnClickListener mSkipNextButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.skipNext();
        }
    };
    private final OnClickListener mStopButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.stop();
        }
    };
    private final OnClickListener mPauseButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.pause();
        }
    };
    private final OnClickListener mPlayButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.play();
        }
    };
    private final OnClickListener mStepForwardButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.stepForward();
        }
    };
    private final OnClickListener mStepBackButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mCommandSender.stepBack();
        }
    };
    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mCommandSender.sendText(s.toString());
        }
    };
    // Send back command to plex
    private View mBackButton;
    private View mKeyboardButton;
    private EditText mHiddenEditText;
    private final OnClickListener mKeyboardButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mHiddenEditText.setText("");
            mHiddenEditText.requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(mHiddenEditText, InputMethodManager.SHOW_FORCED);
        }
    };
    private int mVolume;
    private TextView mVolumeTextView;
    private boolean mAutoPause = false;
    private TelephonyManager mTelephonyManager;
    private boolean mUseAndroidBackButton, museTouchPadPrefs, mPlaceBackButtonToRight;
    private boolean mRestartActivity = false;
    private AlertDialog mFirstTimeInfoAlertDialog;
    private CommandSender mCommandSender;
    private PlexServer mPlexServer;
    private PlexClient mPlexClient;

    public static Intent getIntent(Context context, PlexServer plexServer, PlexClient plexClient) {
        Intent intent = new Intent(context, RemoteActivity.class);
        intent.putExtra(INTENT_PLEX_CLIENT, plexClient);
        intent.putExtra(INTENT_PLEX_SERVER, plexServer);
        return intent;
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPlexClient = (PlexClient) getIntent().getSerializableExtra(INTENT_PLEX_CLIENT);
        mPlexServer = (PlexServer) getIntent().getSerializableExtra(INTENT_PLEX_SERVER);

        mSharedPref.registerOnSharedPreferenceChangeListener(this);

        // NotificationBar
        configureShowNotificationBar();
        configureScreenOrientationAndSetContentView();
        configureKeepScreenOn();

        addPlaybackControls();
        initializeButtonsAndListeners();

        mVolume = mSharedPref.getInt(Constants.PREFS_VOLUME, 48);
        mVolumeTextView.setText(getResources().getString(R.string.volume) + " "
                + mVolume + "%");
    }

    private void initializeButtonsAndListeners() {
        mBackButton = findViewById(R.id.button_back);
        View skipPreviousButton = findViewById(R.id.button_r_skip);
        View rewindButton = findViewById(R.id.button_rv);
        View fastForwardButton = findViewById(R.id.button_ff);
        View skipNextButton = findViewById(R.id.button_f_skip);
        View stopButton = findViewById(R.id.button_stop);
        View pauseButton = findViewById(R.id.button_pause);
        View playButton = findViewById(R.id.button_play);
        View stepForwardButton = findViewById(R.id.button_step_forward);
        View stepBackButton = findViewById(R.id.button_step_back);
        mKeyboardButton = findViewById(R.id.button_show_keyboard);

        mVolumeTextView = (TextView) findViewById(R.id.textView_volume);
        mHiddenEditText = (EditText) findViewById(R.id.invisible_edittext);
        mHiddenEditText.addTextChangedListener(mTextWatcher);
        // Set listeners to buttons
        mBackButton.setOnClickListener(mBackButtonOnClickListener);

        if (skipPreviousButton != null) {
            skipPreviousButton.setOnClickListener(mSkipPreviousButtonOnClickListener);
        }
        if (rewindButton != null) {
            rewindButton.setOnClickListener(mRewindButtonOnClickListener);
        }
        if (fastForwardButton != null) {
            fastForwardButton.setOnClickListener(mFastForwardButtonOnClickListener);
        }
        if (skipNextButton != null) {
            skipNextButton.setOnClickListener(mSkipNextButtonOnClickListener);
        }
        stopButton.setOnClickListener(mStopButtonOnClickListener);
        if (pauseButton != null) {
            pauseButton.setOnClickListener(mPauseButtonOnClickListener);
        }
        playButton.setOnClickListener(mPlayButtonOnClickListener);
        stepForwardButton.setOnClickListener(mStepForwardButtonOnClickListener);
        stepBackButton.setOnClickListener(mStepBackButtonOnClickListener);
        if (!mPlexClient.supportsPlexRemoteProtocol()) {
            mKeyboardButton.setVisibility(View.GONE);
        } else {
            mKeyboardButton.setOnClickListener(mKeyboardButtonOnClickListener);
        }
    }

    private void addPlaybackControls() {
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout ppsLayout = (LinearLayout) findViewById(R.id.linearLayout_pps);
        LinearLayout skipButtonsLayout = (LinearLayout) findViewById(R.id.linearLayout_skipbuttons);

        if (mPlexClient.supportsPlexRemoteProtocol()) {
            inflater.inflate(R.layout.component_small_step_buttons, skipButtonsLayout, true);
        } else {
            inflater.inflate(R.layout.component_small_step_and_search_buttons, skipButtonsLayout, true);
        }
        inflater.inflate(R.layout.component_play_pause_stop, ppsLayout, true);
    }

    private void configureScreenOrientationAndSetContentView() {
        // ===============================================
        // Allow landscape or lock to portrait
        // ===============================================
        boolean allowLandscapeMode = mSharedPref.getBoolean(
                Constants.PREFS_KEY_ALLOW_LANDSCAPE_MODE, false);

        int screenSize = getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK;

        if (!allowLandscapeMode
                && !(screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_remotecontrol);
    }

    private void configureShowNotificationBar() {
        if (mSharedPref.getBoolean(Constants.PREFS_KEY_HIDE_NOTIFICATION_BAR, false)) {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    private void configureKeepScreenOn() {
        if (mSharedPref.getBoolean(Constants.PREFS_KEY_KEEP_SCREEN_ON, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.w(TAG, "RemoteActivity onStart");
        if (mRestartActivity) {
            restartActivity();
        } else {
            boolean useDoubleTap = mSharedPref.getBoolean(
                    Constants.PREFS_KEY_DOUBLE_TAP_FOR_BACK, false);

            museTouchPadPrefs = mSharedPref.getBoolean(
                    Constants.PREFS_KEY_TOUCHPAD, true);

            mUseAndroidBackButton = mSharedPref.getBoolean(
                    Constants.PREFS_KEY_BACKBUTTON, false);

            mPlaceBackButtonToRight = mSharedPref.getBoolean(
                    Constants.PREFS_KEY_BACKBUTTON_POS, false);

            boolean useEscapeForBack = mSharedPref.getBoolean(
                    Constants.PREFS_KEY_USE_ESCAPE, false);

            mCommandSender = CommandSenderImpl.newInstance(mPlexServer, mPlexClient, this);

            mCommandSender.setUseEscapeAsBack(useEscapeForBack);

            configureBackButton(useDoubleTap);
            configureAutoPause();
            configureHomeAsUpEnabled();
            configureNavigationFragment(useDoubleTap);
            showFirstTimeInfoAlert();
        }
        Log.w(TAG, "End of onStart");
    }

    private boolean useTouchPad() {
        return museTouchPadPrefs;
    }

    @SuppressLint("NewApi")
    private void configureHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureNavigationFragment(boolean useDoubleTap) {
        AbstractNavigationFragment navigationsFragment;
        String tag;
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (useTouchPad()) {
            tag = TAG_TOUCHPAD_FRAGMENT;
            navigationsFragment = (AbstractNavigationFragment) fragmentManager.findFragmentByTag(tag);
            if (navigationsFragment == null) {
                navigationsFragment = JoystickTouchpadFragment.newInstance(useDoubleTap);
            } else {
                ((JoystickTouchpadFragment) navigationsFragment).setUseDoubleTap(useDoubleTap);
            }
        } else {
            tag = TAG_NAVBUTTONS_FRAGMENT;
            navigationsFragment = (AbstractNavigationFragment) fragmentManager.findFragmentByTag(tag);
            if (navigationsFragment == null) {
                navigationsFragment = NavigationButtonsFragment.newInstance();
            }
        }

        navigationsFragment.setAsyncFailureCallback(this);
        navigationsFragment.setCommandSender(mCommandSender);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.linearLayout_fragmentPlaceholder, navigationsFragment, tag);
        fragmentTransaction.commit();
    }

    private void configureAutoPause() {
        boolean newAutoPause = mSharedPref.getBoolean(Constants.PREFS_KEY_AUTOPAUSE, false);
        // went from on to off
        if (!newAutoPause && mAutoPause) {
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        // Went from off to on, or if it is the start of the application,
        // mAutoPause will be set to false
        else if (newAutoPause && !mAutoPause) {
            mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        mAutoPause = newAutoPause;
    }

    private void configureActionBarTitle() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (mUseAndroidBackButton) {
                actionBar.setTitle(getResources().getString(
                        R.string.remoteActivity_quit));
            } else {
                actionBar.setTitle(getResources().getString(
                        R.string.app_name));
            }
        }
    }

    @SuppressLint("NewApi")
    private void configureBackButton(boolean useDoubleTap) {
        configureActionBarTitle();
        // Android Back Button
        if (mUseAndroidBackButton || (useTouchPad() && useDoubleTap)) {
            // Always hide the back button
            mBackButton.setVisibility(View.GONE);
            // Not using android back, and not double tapping
        } else {
            mBackButton.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams backButtonParams = (RelativeLayout.LayoutParams) mBackButton.getLayoutParams();
            RelativeLayout.LayoutParams keyboardButtonParams = (RelativeLayout.LayoutParams) mKeyboardButton.getLayoutParams();

            if (backButtonParams != null && keyboardButtonParams != null) {
                if (mPlaceBackButtonToRight) {
                    backButtonParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    keyboardButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                } else {
                    backButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    keyboardButtonParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                }
                mBackButton.setLayoutParams(backButtonParams);
                mKeyboardButton.setLayoutParams(keyboardButtonParams);
            }
        }
    }

    @Override
    public void onFailure(Throwable exception) {

        if (!isFinishing()) {
//			displayConnectionErrorToast();
            try {
                throw exception;
            } catch (ConnectionException e) {
                Log.w(TAG, "ConnectionException: " + e.getMessage());
            } catch (IllegalArgumentException e) {

                Log.d(TAG, "Illegal argument: " + e.getMessage());
            } catch (IllegalStateException e) {

                Log.d(TAG, "IllegalStateException: " + e.getMessage());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mHiddenEditText.getWindowToken(), 0);
        super.onPause();
    }

    @Override
    protected void onStop() {
        PlexRemoteApp.getApp().getVolleyRequestQueue().cancelAll(this);
        Log.d(TAG, "RemoteActivity onStop");
        storeVolume();

        if (mCommandSender != null) {
            mCommandSender.shutdown();
            mCommandSender = null;
        }

        SectionHelper.dismissDialogs();

        if (mFirstTimeInfoAlertDialog != null) {
            mFirstTimeInfoAlertDialog.dismiss();
            mFirstTimeInfoAlertDialog = null;
        }

        if (mAutoPause) {
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (mPlexClient.supportsPlexRemoteProtocol()) {
            inflater.inflate(R.menu.remote_activity_new_api, menu);
        } else {
            inflater.inflate(R.menu.remote_activity_old_api, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_update_section) {
            SectionHelper.getAndRefreshSection(this, mPlexServer, this);
            return true;
        }
        if (id == R.id.menu_settings) {
            Intent intent = new Intent(RemoteActivity.this,
                    AppPreferenceActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.menu_subtitles_toggle) {
            mCommandSender.toggleSubtitle();
            return true;
        }
        if (id == R.id.menu_subtitles_language) {
            mCommandSender.subtitleLanguage();
            return true;
        }
        if (id == R.id.menu_audio_language) {
            mCommandSender.audioLanguage();
            return true;
        }
        if (id == R.id.menu_codec_info) {
            mCommandSender.showCodecInfo();
            return true;
        }
        if (id == R.id.menu_osd) {
            mCommandSender.showOSD();
            return true;
        }
        if (id == R.id.menu_menu) {
            mCommandSender.showMenu();
            return true;
        }
        if (id == R.id.menu_queue) {
            mCommandSender.queue();
            return true;
        }
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (mVolume <= 96) {
                        mVolume += 4;
                        mCommandSender.setVolume(mVolume);
                    }
                }
                mVolumeTextView.setText(getResources().getString(R.string.volume)
                        + " " + mVolume + "%");
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (mVolume >= 4) {

                        mVolume -= 4;
                        mCommandSender.setVolume(mVolume);

                    }
                }
                mVolumeTextView.setText(getResources().getString(R.string.volume)
                        + " " + mVolume + "%");
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public void onBackPressed() {
        if (mUseAndroidBackButton) {
            mCommandSender.back();
        } else {
            super.onBackPressed();
        }
    }

    private void showFirstTimeInfoAlert() {
        final String prefs_key;
        boolean showDialog;

        if (useTouchPad()) {
            prefs_key = Constants.PREFS_KEY_SHOW_TOUCHPAD_HOW_TO;
        } else {
            prefs_key = Constants.PREFS_KEY_SHOW_NAVBUTTONS_HOW_TO;
        }

        showDialog = mSharedPref.getBoolean(prefs_key, true);
        if (showDialog) {
            int textId, titleId;
            if (useTouchPad()) {
                textId = R.string.touchpad_howto_text;
                titleId = R.string.touchpad_howto_title;
            } else {
                textId = R.string.navbuttons_howto_text;
                titleId = R.string.navbuttons_howto_title;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    RemoteActivity.this);
            builder.setTitle(titleId);
            builder.setMessage(textId);

            builder.setNegativeButton(
                    getResources().getString(R.string.dontShowAgain),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            mEditor.putBoolean(prefs_key, false);
                            mEditor.commit();
                        }
                    }
            );
            builder.setNeutralButton(
                    getResources().getString(R.string.showAgain),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    }
            );

            mFirstTimeInfoAlertDialog = builder.create();
            mFirstTimeInfoAlertDialog.show();
        }
    }

    private void storeVolume() {
        mEditor.putInt(Constants.PREFS_VOLUME, mVolume);
        mEditor.commit();
    }

    private void displayConnectionErrorToast() {
        makeShortToast(getResources().getString(R.string.serverConnectionError));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        if (key.equals(Constants.PREFS_KEY_ALLOW_LANDSCAPE_MODE)
                || key.equals(Constants.PREFS_KEY_HIDE_NOTIFICATION_BAR)
                || key.equals(Constants.PREFS_KEY_KEEP_SCREEN_ON)
                || key.equals(Constants.PREFS_KEY_BACKBUTTON_POS)) {
            mRestartActivity = true;
        } else if (key.equals(Constants.PREFS_KEY_DOUBLE_TAP_FOR_BACK)) {

        }
    }

    private void restartActivity() {

        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();

        overridePendingTransition(0, 0);
        startActivity(intent);
    }

}
