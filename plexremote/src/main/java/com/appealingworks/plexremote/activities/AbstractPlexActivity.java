/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.appealingworks.plexremote.util.Log;

public abstract class AbstractPlexActivity extends AppCompatActivity {
    private static final String TAG = AbstractPlexActivity.class.getSimpleName();
    private final Object mSyncObj = new Object();
    protected Handler mHandler;
    protected SharedPreferences mSharedPref;
    protected SharedPreferences.Editor mEditor;
    private Toast mToast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSharedPreferences();

    }

    private void initSharedPreferences() {
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        mEditor = mSharedPref.edit();
    }

    @SuppressLint("ShowToast")
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "AbstractPlexActivity onStart");

        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "AbstractPlexActivity onStop");

        synchronized (mSyncObj) {
            if (mToast != null) {
                mToast.cancel();
                mToast = null;
            }
        }

        super.onStop();
    }

    protected final void makeShortToast(final String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Time to show toast: " + msg);

                synchronized (mSyncObj) {
                    // Ugly fix to prevent NullPointerException when
                    // RemoteActivity
                    // is started in landscape mode on a small screen
                    if (mToast != null) {
                        mToast.setText(msg);
                        mToast.show();
                    }
                }
            }
        });

    }
}
