/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.appealingworks.plexremote.PlexRemoteApp;
import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.net.NetworkUtils;
import com.appealingworks.plexremote.util.Constants;
import com.appealingworks.plexremote.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

public abstract class AbstractFindActivity extends AbstractPlexActivity {

    private static final String TAG = AbstractFindActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int screenSize = getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK;

        if (screenSize != Configuration.SCREENLAYOUT_SIZE_XLARGE
                && screenSize != Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    // Convenience methods, abstracting away connectionManager
    protected boolean isWiFiConnected() {
        return getWifiNetworkInfo().isConnected();
    }

    protected boolean ignoreWiFiConnectionState() {
        return mSharedPref.getBoolean(Constants.PREFS_KEY_DISABLE_WIFI_CHECK,
                false);
    }

    protected InetAddress getBroadcastAddress() {
        DhcpInfo dhcpInfo = getDhcpInfo();

        try {
            if (dhcpInfo != null) {
                int broadcast = NetworkUtils.calculateBroadcastAddress(
                        dhcpInfo.netmask, dhcpInfo.ipAddress);
                return NetworkUtils.intToInetAddress(broadcast);
            }
        } catch (UnknownHostException e) {
            Log.d(TAG, "Could not resolve broadcast address from int");
        }

        try {
            InetAddress address = InetAddress.getByName("255.255.255.255");
            Log.d(TAG, "Defaulting to broadcast address"
                    + address.getHostAddress());
            return address;
        } catch (UnknownHostException e) {
            Log.d(TAG,
                    "Failed turning string \"255.255.255.255\" to InetAddress");
        }
        Log.i(TAG, "Could not get a broadcast address");
        return null;

    }

    protected void informAboutWifiState() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.noWiFiTitle));
        builder.setMessage(getResources().getString(R.string.noWiFiMessage));

        builder.setNeutralButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }
        );

        builder.setPositiveButton(
                getResources().getString(R.string.wiFiSettings),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                }
        );

        AlertDialog alert = builder.create();

        alert.show();
    }

    @Override
    protected void onStop() {
        PlexRemoteApp.getApp().getVolleyRequestQueue().cancelAll(this);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.only_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_settings) {
            Intent intent = new Intent(AbstractFindActivity.this,
                    AppPreferenceActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private DhcpInfo getDhcpInfo() {
        DhcpInfo dhcpInfo = null;

        WifiManager wifiManager = ((WifiManager) this
                .getSystemService(Context.WIFI_SERVICE));

        if (wifiManager != null) {
            dhcpInfo = wifiManager.getDhcpInfo();
        }
        return dhcpInfo;
    }

    private NetworkInfo getWifiNetworkInfo() {
        NetworkInfo networkInfo = null;
        ConnectivityManager connectivityManager = ((ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE));

        if (connectivityManager != null) {
            networkInfo = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        return networkInfo;
    }
}
