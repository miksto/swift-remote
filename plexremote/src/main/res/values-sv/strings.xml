<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright 2014 Mikael Stockman
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<resources>

    <string name="app_name">Swift Remote</string>

    <string name="manualTitle">Manuella inställningar</string>
    <string name="ipInput">Serverns IP-adress</string>
    <string name="portNumberInput">Serverns portnummer (Standard: 32400)</string>
    <string name="clientSpinner">Tillgängliga plexspelare</string>
    <string name="findClientsButton">Hitta spelare</string>
    <string name="done">Klar</string>
    <string name="autoFindHeader">Välj server och spelare</string>
    <string name="autoFindServerPicker">Välj server</string>
    <string name="autoFindClientPicker">Välj spelare</string>
    <string name="autoFindRefresh">Uppdatera</string>
    <string name="autoFindManual">Manuellt</string>

    <string name="navbuttons_howto_text">Håll nere en pilknapp för att snabb-bläddra i menyerna.\n\nDen runda knappen i mitten fungerar som enter.\n\nAnvänd volymknaparna på telefonen för att ändra volymen för Plex.</string>
    <string name="touchpad_howto_text">Dra fingret över pekplattan för att gå upp/ner/vänster/höger i Plex\n\nFör att snabb-bläddra i menyerna, sätt ett finger på pekplattan, dra det en liten bit, och håll still ett kort tag, så kommer snabb-bläddringen att starta. Ju längre du drar, desto snabbare kommer du att bläddra.\n\nOm du vill dubbeldutta på pekplattan för att gå tillbaka i Plex, gå till inställningar och aktivera \"Pekplatta dubbeldutt\"\n\nAnvänd volymknaparna på telefonen för att ändra volymen för Plex.</string>
    <string name="navbuttons_howto_title">Navigeringsknapparna!</string>
    <string name="touchpad_howto_title">Pekplattan!</string>

    <string name="searching">Ansluter&#8230;</string>
    <string name="serverConnectionError">Kunde inte ansluta till servern</string>
    <string name="noClientsFound">Plex servern returnerade 0 spelare.</string>
    <string name="clientsFound"> spelare hittade.</string>
    <string name="oneClientFound">En spelare hittad.</string>
    <string name="noWiFiMessage">Du är inte ansluten till ett trådlöst nätverk. För att fjärrkontrollen ska fungera behöver din telefon vara ansluten till samma nätverk som servern.</string>
    <string name="volume">Volym: </string>
    <string name="close">Stäng</string>
    <string name="noWiFiTitle">Ingen Wi-Fi anslutning!</string>
    <string name="wiFiSettings">Wi-Fi inställningar</string>
    <string name="dontShowAgain">Visa inte detta igen</string>
    <string name="showAgain">Påminn mig igen</string>
    <string name="refreshing">Uppdateras</string>
    <string name="noSections">Kunde inte hittan några sektioner</string>
    <string name="invalidPortNumber">Ogiltigt portnummer</string>

    <string name="remoteActivity_quit">Avsluta</string>

    <string name="menu_update_section">Uppdatera sektion</string>
    <string name="menu_settings">Inställningar</string>
    <string name="menu_subtitles_toggle">Undertext (På/Av) (s)</string>
    <string name="menu_subtitles_language">Undertextspråk (l)</string>
    <string name="menu_codec_info">Kodek-information (i)</string>
    <string name="menu_audio_language">Ljudspråk (a)</string>
    <string name="menu_osd">OSD (m)</string>
    <string name="menu_menu">Meny (c)</string>
    <string name="menu_queue">Köa (q)</string>

    <string name="prefs_category_features_title">Funktioner</string>
    <string name="prefs_touchPad_summary">Använd en pekplatta för att navigera i menyerna</string>
    <string name="prefs_touchPad_title">Pekplatta</string>
    <string name="prefs_autopause_summary">Pausa automatiskt om någon ringer</string>
    <string name="prefs_autopause_title">Autopaus</string>


    <string name="prefs_category_screen_title">Skärm</string>
    <string name="prefs_allowLandscapeMode_summary">Tillåt landskapsläge för all skärmstorlekar (Layouten kan bli felaktig på för små skärmar)</string>
    <string name="prefs_allowLandscapeMode_title">Landskapsläge</string>
    <string name="prefs_keepScreenOn_summary">Stäng inte av skärmen vid inaktivitet</string>
    <string name="prefs_keepScreenOn_title">Skärm alltid på</string>
    <string name="prefs_hideNotificationBar_summary">Dölj notifieringsfältet i fjärrkontroll-vyn</string>
    <string name="prefs_hideNotificationBar_title">Helskärmsläge</string>


    <string name="prefs_category_backbutton_title">Bakåtknapp</string>
    <string name="prefs_use_android_back_button_summary">Använd Androids bakåtknapp för att gå tillbaka i Plex</string>
    <string name="prefs_use_android_back_button_title">Androids bakåtknapp</string>
    <string name="prefs_backButton_pos_summary">Placera bakåtknappen till höger</string>
    <string name="prefs_backButton_pos_title">Placering av bakåtknapp</string>
    <string name="prefs_useEscape_summary">Simulera att escape-tangenten trycks när bakåtknappen används om detta stöds av spelaren</string>
    <string name="prefs_useEscape_title">Simulera escape-tangent</string>
    <string name="prefs_double_tap_to_go_back_summary">Dubbeldutta på pekplattan för att gå tillbaka i Plex</string>
    <string name="prefs_double_tap_to_go_back_title">Pekplatta dubbeldutt</string>

    <string name="prefs_category_other_settings_title">Andra inställningar</string>
    <string name="prefs_disableWiFiCheck_summary">Kräv inte en Wi-Fi-anslutning vid sökning av Plexspelare eller servrar</string>
    <string name="prefs_disableWiFiCheck_title">Inaktivera Wi-Fi kontroll</string>


    <string name="prefs_category_about_title">Om applikationen</string>
    <string name="prefs_version_title">Version</string>
    <string name="activity_autofind_problems"><u>Problem? Kolla denna felsökningssida!</u></string>
    <string name="choose_mode">Välj sätt:</string>

</resources>
