/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.model;


import android.test.AndroidTestCase;

/**
 * Created by Mikael Stockman on 14/12/13.
 */
public class PlexVersionTest extends AndroidTestCase {

    public void testBasicFunctionality() {
        //Identical version
        PlexVersion versionA = new PlexVersion("");
        PlexVersion versionB = new PlexVersion("");


        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("0.9.8.8");
        assertEquals(versionA.compareTo(versionB), 0);
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertTrue(versionB.isGreaterOrEqualTo(versionA));

        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("0.9.8.8-d84jf83h");
        assertEquals(versionA.compareTo(versionB), 0);
        assertEquals(versionB.compareTo(versionA), 0);

        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("0.9.8.9");
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
        assertFalse(versionA.isGreaterOrEqualTo(versionB));

        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("0.9.8.9-fg356");
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
        assertFalse(versionA.isGreaterOrEqualTo(versionB));

        versionA.setVersionString("0.9.8.8-38djf");
        versionB.setVersionString("0.9.8.9-fg356");
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
        assertFalse(versionA.isGreaterOrEqualTo(versionB));

        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("1");
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
        assertFalse(versionA.isGreaterOrEqualTo(versionB));

        versionA.setVersionString("1.2");
        versionB.setVersionString("1.11");
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
        assertFalse(versionA.isGreaterOrEqualTo(versionB));
    }

    public void testDifferentVersionNameLengths() {
        PlexVersion versionA = new PlexVersion("0.9.8.8");
        PlexVersion versionB = new PlexVersion("0.9.8");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        versionA.setVersionString("0.9.8.8");
        versionB.setVersionString("0.9");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        versionB.setVersionString("0");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        versionB.setVersionString("0-je85h");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        //1.0.0.0 == 1
        versionA.setVersionString("1");
        versionB.setVersionString("1.0.0.0");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
    }

    public void testInvalidVersionNumbers() {
//		a version is always greater than null
        PlexVersion versionA = new PlexVersion("0.9.8.8");
        PlexVersion versionB = new PlexVersion(null);
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        versionA = new PlexVersion("0.9.8.8");
        versionB = null;
        assertTrue(versionA.isGreaterOrEqualTo(versionB));

        versionA = new PlexVersion("1");
        versionB = new PlexVersion("1.fdjk");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertTrue(versionB.isGreaterOrEqualTo(versionA));

        versionA = new PlexVersion("1");
        versionB = new PlexVersion("fdjk");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertFalse(versionB.isGreaterOrEqualTo(versionA));

        versionA = new PlexVersion("");
        versionB = new PlexVersion("");
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertTrue(versionB.isGreaterOrEqualTo(versionA));

        versionA = new PlexVersion(null);
        versionB = new PlexVersion(null);
        assertTrue(versionA.isGreaterOrEqualTo(versionB));
        assertTrue(versionB.isGreaterOrEqualTo(versionA));
    }
}
